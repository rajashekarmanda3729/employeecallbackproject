const { readAFile, retriveDataForIds, groupDataBasedOnCompanies,
    getDataFromCompnay, removeDetailsOfId, sortData, swapElementPositions,
    addNewKeyAndValueToData } = require('./solutionEmployeeCallBack.cjs')


readAFile('data.json', (err, data) => {
    if (err) {
        console.log(err)
    } else {
        retriveDataForIds([2, 13, 28], data, (err, data) => {
            if (err) {
                console.log(err)
            } else {
                groupDataBasedOnCompanies(data, (err, data) => {
                    if (err) {
                        console.log(err)
                    } else {
                        getDataFromCompnay(data, (err, data) => {
                            if (err) {
                                console.log(err)
                            } else {
                                removeDetailsOfId(2,data, (err, data) => {
                                    if (err) {
                                        console.log(err)
                                    } else {
                                        sortData(data, (err, data) => {
                                            if (err) {
                                                console.log(err)
                                            } else {
                                                swapElementPositions(data, (err, data) => {
                                                    if (err) {
                                                        console.log(err)
                                                    } else {
                                                        addNewKeyAndValueToData(data, (err, data) => {
                                                            if (err) {
                                                                console.log(err)
                                                            } else {
                                                                console.log(data)
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})